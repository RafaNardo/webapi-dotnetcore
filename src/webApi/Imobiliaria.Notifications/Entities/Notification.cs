namespace WebApi.Notifications.Entities 
{
    public class Notification
    {
        public long Id { get; private set; }

        public string Description { get; private set; }
        
        public Notification(string description)
        {
            Description = description;
        }

        public Notification(long id, string description)
        {
            Id = id;
            Description = description;
        }
    }
}