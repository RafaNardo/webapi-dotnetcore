using WebApi.Notifications.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace WebApi.Notifications.Entities 
{
    public abstract class Notifable : INotifable
    {
        private List<Notification> _notifications => _notifications ?? new List<Notification>();

        public IReadOnlyCollection<Notification> Notifications => _notifications;

        public bool IsValid => !_notifications.Any();

        public void AddNotification(Notification notification)
        {
            _notifications.Add(notification);
        }
        
        public void AddNotification(string message)
        {
            _notifications.Add(new Notification(message));
        }
    }
}