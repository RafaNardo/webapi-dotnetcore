using System.Collections.Generic;

namespace WebApi.Notifications.Interfaces
{
    public interface INotifable 
    {
        bool IsValid { get; }

        IReadOnlyCollection<Notifications.Entities.Notification> Notifications { get; }

        void AddNotification(Notifications.Entities.Notification notification);
        
        void AddNotification(string message);
    }
}