﻿using WebApi.Notifications.Interfaces;
using System;

namespace WebApi.Notifications.Contracts
{
    public class ValidationContract<TContract> where TContract : INotifable
    {
        private readonly TContract _contract;

        public ValidationContract(TContract contract)
        {
            _contract = contract;
        }
        public ValidationContract<TContract> IsNotNull<T>(Func<TContract, T> selector, string field) where T : IComparable
        {
            return IsNotNull(selector, field, true);
        }
        
        public ValidationContract<TContract> IsNotNull<T>(Func<TContract, T> selector, string field, bool condition) where T : IComparable
        {
            if (!condition)
                return this;
                
            if (selector(_contract) == null || selector(_contract).Equals(default(T)))
            {
                _contract.AddNotification($"O campo \"{field}\" é obrigatório.");
            }

            return this;
        }

        public ValidationContract<TContract> IsGreaterThan<T>(Func<TContract, T> selector, T value, string field, bool condition) where T : IComparable
        {
            if (!condition || selector(_contract) == null)
                return this;

            if (selector(_contract).CompareTo(value) <= 0)
            {
                _contract.AddNotification($"O campo \"{field}\" deve ser maior que \"{value.ToString()}\".");
            }

            return this;
        }
    }
}
