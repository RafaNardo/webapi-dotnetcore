﻿namespace WebApi.Application.Base.Commands
{
    public abstract class CommandResult
    {
        public bool Sucess { get; set; }

        public static CommandResult DefaultCommandResult => new DefaultCommandResult();
    }
}
