﻿using System.Linq;
using WebApi.Application.Base.Commands;
using WebApi.Application.Customers.Dtos;
using WebApi.Domain.Customers.Entities;
using WebApi.Domain.Repositories;
using WebApi.Domain.Customers.Validators;
using WebApi.Application.Customers.Commands;
using WebApi.Application.Customers.CommandResults;

namespace WebApi.Application.Customers
{
    public class CustomerAppService : ICustomerAppService
    {
        private readonly ICustomerRepository _customerRepository;

        public CustomerAppService(ICustomerRepository customerRepository)
        {
            _customerRepository = customerRepository;
        }

        public CommandResult GetAll()
        {
            var customers = _customerRepository
                                .GetAll()
                                .Select(c => new CustomerDto(c));

            return new ListCustomerCommandResult(customers);
        }

        public CommandResult GetById(int id)
        {
            var customer = _customerRepository.GetById(id);

            return new GetCustomerCommandResult(new CustomerDto(customer));
        }

        public CommandResult Create(CreateCustomerCommand command)
        {
            var customer = new Customer(command.Customer.Name, 
                                        command.Customer.Email, 
                                        command.Customer.BirthDate);

            var validator = new CustomerValidator().Validate(customer);

            if (validator.IsValid)
                _customerRepository.Create(customer);

            return CommandResult.DefaultCommandResult;
        }

        public CommandResult Update(UpdateCustomerCommand command)
        {
            var customer = new Customer(command.Customer.Name,
                                        command.Customer.Email,
                                        command.Customer.BirthDate);

            var validator = new CustomerValidator().Validate(customer);

            if (validator.IsValid)
                _customerRepository.Update(customer);

            return CommandResult.DefaultCommandResult;
        }

        public CommandResult Delete(int id)
        {
            _customerRepository.Delete(id);

            return CommandResult.DefaultCommandResult;
        }
    }
}
