﻿using WebApi.Application.Customers.Dtos;
using WebApi.Application.Base.Commands;

namespace WebApi.Application.Customers.CommandResults
{
    public class GetCustomerCommandResult : CommandResult
    {
        public GetCustomerCommandResult(CustomerDto customer) => Customer = customer;

        public CustomerDto Customer { get; set; }
    }
}
