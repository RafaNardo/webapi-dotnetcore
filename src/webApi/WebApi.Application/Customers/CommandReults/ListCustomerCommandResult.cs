﻿using System.Collections.Generic;
using WebApi.Application.Customers.Dtos;
using WebApi.Application.Base.Commands;

namespace WebApi.Application.Customers.CommandResults
{
    public class ListCustomerCommandResult : CommandResult
    {
        public ListCustomerCommandResult(IEnumerable<CustomerDto> customers) => Customers = customers;

        public IEnumerable<CustomerDto> Customers { get; set; }
    }
}
