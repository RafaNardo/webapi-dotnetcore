﻿using System;
using WebApi.Domain.Customers.Entities;

namespace WebApi.Application.Customers.Dtos
{
    public class CustomerDto
    {
        public CustomerDto()
        {

        }

        public CustomerDto(Customer customer)
        {
            Id = customer.Id;
            Name = customer.Name;
            Email = customer.Email;
            BirthDate = customer.BirthDate;
        }

        public int Id { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public DateTime BirthDate { get; set; }
    }
}
