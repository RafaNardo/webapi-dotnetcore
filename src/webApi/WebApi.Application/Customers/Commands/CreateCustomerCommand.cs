﻿using System;
using WebApi.Application.Base.Commands;
using WebApi.Application.Customers.Dtos;

namespace WebApi.Application.Customers.Commands
{
    public class CreateCustomerCommand : Command
    {
        public CustomerDto Customer { get; set; }
    }
}
