﻿using WebApi.Application.Base.Commands;
using WebApi.Application.Customers.Commands;

namespace WebApi.Application.Customers
{
    public interface ICustomerAppService
    {
        CommandResult GetAll();

        CommandResult GetById(int id);

        CommandResult Create(CreateCustomerCommand command);

        CommandResult Update(UpdateCustomerCommand command);

        CommandResult Delete(int id);
    }
}
