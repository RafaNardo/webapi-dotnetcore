﻿using Microsoft.AspNetCore.Mvc;
using WebApi.Application.Customers;
using WebApi.Application.Customers.Commands;

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        private ICustomerAppService _customerAppService;

        public CustomersController(ICustomerAppService customerAppService)
        {
            _customerAppService = customerAppService;
        }

        // GET: api/Customers
        [HttpGet]
        public IActionResult Get()
        {
            var result = _customerAppService.GetAll();

            return Ok(result);
        }

        // GET: api/Customers/5
        [HttpGet("{id}", Name = "Get")]
        public IActionResult Get(int id)
        {
            var result = _customerAppService.GetById(id);

            return Ok(result);
        }

        // POST: api/Customers
        [HttpPost]
        public IActionResult Post([FromBody] CreateCustomerCommand command)
        {
            var result = _customerAppService.Create(command);

            if (result.Sucess)
            {
                return Ok(result);
            }
            else
            {
                return BadRequest(result);
            }

        }

        // PUT: api/Customers/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] UpdateCustomerCommand command)
        {
            command.Customer.Id = id;

            var result = _customerAppService.Update(command);

            if (result.Sucess)
            {
                return Ok(result);
            }
            else
            {
                return BadRequest(result);
            }
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var result = _customerAppService.Delete(id);

            if (result.Sucess)
            {
                return Ok(result);
            }
            else
            {
                return BadRequest(result);
            }
        }
    }
}
