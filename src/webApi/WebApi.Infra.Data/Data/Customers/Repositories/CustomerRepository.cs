﻿using System;
using System.Collections.Generic;
using Dapper;
using System.Linq;
using WebApi.Domain.Customers.Entities;
using WebApi.Domain.Repositories;
using WebApi.Infra.Data.Customers.Sql;
using Microsoft.Extensions.Configuration;

namespace WebApi.Infra.Data.Customers.Repositories
{
    public class CustomerRepository : RepositoryBase, ICustomerRepository
    {
        private readonly ICollection<Customer> _customers;

        public CustomerRepository(IConfiguration configuration) : base(configuration)
        {
            _customers = new List<Customer>
            {
                new Customer(1, "Rafael", "rafael@gmail.com", new DateTime(1992, 05, 12)),
                new Customer(2, "John", "john@gmail.com", new DateTime(1991, 06, 13)),
                new Customer(3, "Carrie", "carrie@gmail.com", new DateTime(1990, 07, 14)),
                new Customer(4, "Joseph", "joseph@gmail.com", new DateTime(1989, 08, 15))
            };
        }

        public IEnumerable<Customer> GetAll()
        {
            //using (Connection)
            //{
            //    return Connection.Query<Customer>(ResourcesCustomerSql.Customer.List);
            //}

            return _customers;

        }

        public Customer GetById(int id)
        {
            //using (Connection)
            //{
            //return Connection.Query<Customer>(ResourcesCustomerSql.Customer.Get).Single();
            //}

            return _customers.SingleOrDefault(customer => customer.Id == id);
        }

        public void Create(Customer customer)
        {
            //using (Connection)
            //{
            //    Connection.ExecuteScalar(
            //        ResourcesCustomerSql.Customer.Create,
            //        customer
            //    );
            //}

            _customers.Add(customer);
        }

        public void Update(Customer customer)
        {
            using (Connection)
            {
                Connection.Execute(
                    ResourcesCustomerSql.Customer.Update, 
                    customer
                );
            }
        }

        public void Delete(int id)
        {
            using (Connection)
            {
                Connection.Execute(
                    ResourcesCustomerSql.Customer.Delete,
                    new
                    {
                        Id = id
                    });
            }
        }
    }
}
