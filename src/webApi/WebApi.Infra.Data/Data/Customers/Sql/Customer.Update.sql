UPDATE 
	Customer
SET
	[Name] = @Name,
	[BirthDate] = @BirthDate,
	[Email] = @Email
WHERE
	Id = @Id