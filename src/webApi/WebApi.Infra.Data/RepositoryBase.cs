﻿using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;

namespace WebApi.Infra.Data
{
    public abstract class RepositoryBase
    {
        private static readonly string connectionName = "WebApiExample";

        protected readonly string ConnectionString;

        private SqlConnection _connection = null;

        protected SqlConnection Connection
        {
            get
            {
                if (null == _connection)
                {
                    _connection = new SqlConnection(this.ConnectionString);
                }

                return _connection;
            }
        }

        protected RepositoryBase(IConfiguration c)
        {
            ConnectionString = c.GetConnectionString(connectionName);
        }
    }
}
