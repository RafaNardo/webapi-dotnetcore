using FluentValidation.TestHelper;
using NUnit.Framework;
using System;
using WebApi.Domain.Customers.Validators;

namespace Tests
{
    [TestFixture]
    public class CustomerValidatorTest
    {
        private CustomerValidator validator;

        [SetUp]
        public void Setup()
        {
            validator = new CustomerValidator();
        }

        #region TestsForName

        [Test]
        public void ShouldNotHaveErrorWhenNameIsSpecified()
        {
            validator.ShouldNotHaveValidationErrorFor(customer => customer.Name, "Some Name");
            Assert.Pass();
        }

        [Test]
        public void ShouldHaveErrorWhenNameIsNull()
        {
            validator.ShouldHaveValidationErrorFor(customer => customer.Name, null as string);
            Assert.Pass();
        }

        [Test]
        public void ShouldHaveErrorWhenNameIsEmpty()
        {
            validator.ShouldHaveValidationErrorFor(customer => customer.Name, string.Empty);
            Assert.Pass();
        }

        #endregion TestsForName

        #region TestsForBirthDate

        [Test]
        public void ShouldNotHaveErrorWhenBirthDateIsSpecified()
        {
            validator.ShouldNotHaveValidationErrorFor(customer => customer.BirthDate, new DateTime(1992, 05, 12));
            Assert.Pass();
        }

        [Test]
        public void ShouldHaveErrorWhenBirthDateIsNull()
        {
            validator.ShouldNotHaveValidationErrorFor(customer => customer.BirthDate, null);
            Assert.Pass();
        }

        [Test]
        public void ShouldHaveErrorWhenBirthDateIsDefault()
        {
            validator.ShouldNotHaveValidationErrorFor(customer => customer.BirthDate, default(DateTime));
            Assert.Pass();
        }

        #endregion TestsForBirthDate
    }
}