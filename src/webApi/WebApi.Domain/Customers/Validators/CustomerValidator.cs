﻿using FluentValidation;
using System;
using WebApi.Domain.Customers.Entities;

namespace WebApi.Domain.Customers.Validators
{
    public class CustomerValidator : AbstractValidator<Customer>
    {
        public CustomerValidator()
        {
            RuleFor(customer => customer.Name)
                .Null()
                .Empty();

            RuleFor(customer => customer.Email)
                .Null()
                .Empty()
                .EmailAddress();

            RuleFor(customer => customer.BirthDate)
                .Null()
                .Must(d => !d.Equals(default(DateTime)));
        }
    }
}
