﻿using System;

namespace WebApi.Domain.Customers.Entities
{
    public class Customer : EntityBase
    {
        public Customer()
        { }

        public Customer(int id, string nome, string email, DateTime dataNascimento) : base(id)
        {
            Id = id;
            Name = nome;
            Email = email;
            BirthDate = dataNascimento;
        }

        public Customer(string name, string email, DateTime birthDate)
        {
            Name = name;
            Email = email;
            BirthDate = birthDate;
        }

        public string Name { get; private set; }

        public string Email { get; private set; }

        public DateTime BirthDate { get; private set; }
    }
}
