﻿using System.Collections.Generic;
using WebApi.Domain.Customers.Entities;

namespace WebApi.Domain.Repositories
{
    public interface ICustomerRepository
    {
        IEnumerable<Customer> GetAll();

        Customer GetById(int id);

        void Create(Customer customer);

        void Update(Customer customer);

        void Delete(int id);
    }
}
