﻿using FluentValidation;
using FluentValidation.Results;

namespace WebApi.Domain
{
    public abstract class EntityBase
    {
        public int Id { get; protected set; }

        protected EntityBase()
        { }

        protected EntityBase(int id) => Id = id;

        public bool IsValid => Validations.IsValid;

        public ValidationResult Validations { get; protected set; }

        public virtual void Validate(IValidator validator)
        {
            Validations = validator.Validate(this);
        }
    }
}
